﻿function CreatePath($path) {
    if (-not (Test-Path $path)){
        ni -ItemType directory -path $path
    }
}

function TestVersion($package, $version) {
    return Test-Path "$package/lib/$version" -PathType Container
}

function TestNoSubFolder($package) {
    return Test-Path "$package/*" -PathType Leaf
}

function GetAssemblyDestination($name) {
    if($name.StartsWith("System.")){
        return "_system"
    }
    if($name.StartsWith("Microsoft.")){
        return "_microsoft"
    }
    if($name.Contains(".Driver")){
        return "_drivers"
    }
    if(-not $name.StartsWith("Paximum.")){
        return "_lib"
    }
    $parts = $name.Split('.')
    return "$($parts[0]).$($parts[1])"
}

function ListDlls($package, $version, $outFolder){
    $folder = (Get-Item "$package/lib/$version")[0];
    Get-ChildItem "$folder/*.dll" | % {
        $destination = GetAssemblyDestination $_.Name
        CreatePath "$outFolder\$destination"
        Copy-Item $_.FullName "$outFolder\$destination"
        $pdb = $_.FullName.Replace(".dll", ".pdb")
        if(Test-Path $pdb) {
            Copy-Item $pdb "$outFolder\$destination"
        }
    } 
}

function InstallFiles($path, $output) 
{ 
    foreach ($package in Get-ChildItem $path)
    {
        if(Test-Path "$package/lib" -PathType Container){
            if(TestVersion $package "net471") {
                ListDlls "$package" "net471" $output
            }
			elseif(TestVersion $package "net462") {
                ListDlls "$package" "net462" $output
            }
            elseif(TestVersion $package "net461") {
                ListDlls "$package" "net461" $output
            }
            elseif(TestVersion $package "net46") {
                ListDlls "$package" "net46" $output
            }
            elseif(TestVersion $package "net452") {
                ListDlls "$package" "net452" $output
            }
            elseif(TestVersion $package "net451") {
                ListDlls "$package" "net451" $output
            }
            elseif(TestVersion $package "net45") {
                ListDlls "$package" "net45" $output
            }         
            elseif(TestVersion $package "net40") {
                ListDlls "$package" "net40" $output
            }
            elseif(TestVersion $package "net35"){
                ListDlls "$package" "net35" $output
            }
			elseif(TestVersion $package "netstandard2.0"){
                ListDlls "$package" "netstandard2.0" $output
            }
			elseif(TestVersion $package "netstandard1.1"){
                ListDlls "$package" "netstandard1.1" $output
            }			
            elseif(TestVersion $package "*net4*"){
                ListDlls "$package" "*net4*" $output
            }
			elseif(TestVersion $package "*net4*"){
                ListDlls "$package" "*net4*" $output
            }
            elseif(TestNoSubFolder $package){
                ListDlls "$package" "" $output
            }            
            else{
                Write-Host "$package : not found"
            }
        }  
    } 
}

function DirtyFixes($path){
    Move-Item "$path/_drivers/MongoDB.Driver.dll" "$path/_lib/"
    Move-Item "$path/_lib/Owin.dll" "$path/"
    Move-Item "$path/_microsoft/Microsoft.Owin.Host.SystemWeb.dll" "$path/"
    Copy-Item "./Aerospike.Client/lib/net/AerospikeClient.dll" "$path/_lib/"
}

InstallFiles ./ ./bin

DirtyFixes ./bin
